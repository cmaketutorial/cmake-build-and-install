cmake_minimum_required( VERSION 3.13.0)

set( PUBLIC_HEADERS 
    "${PUBLIC_HEADERS}"
    "${CMAKE_CURRENT_SOURCE_DIR}/IStringnify.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/MyStringnifyInterface.hpp"
    PARENT_SCOPE
)
