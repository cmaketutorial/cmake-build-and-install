#pragma once

#include <memory>
#include <string>

#include "mystringnify_export.h"

namespace MyString {

class IStringnify;

/**
 * @brief Constructs a SimpleStringnifyer which adds a prefix and postfix to the output.
 * 
 * @param tPrefix Optional prefix
 * @param tPostfix Optional postfix
 * @return SimpleString instance.
 */
std::unique_ptr<IStringnify> MYSTRINGNIFY_EXPORT makeSimpleStringnifyer(const std::string& tPrefix = "",
    const std::string& tPostfix = "");

}