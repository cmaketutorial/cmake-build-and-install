#pragma once

#include <string>
#include <memory>

#include "mystringnify_export.h"

/**
 * @brief Namespace encapsulating all the functions in the library.
 * 
 */
namespace MyString {

/**
 * @brief Interface of the multiple stringnifier strategies.
 * Allows to transform an arbitrary input into a formated string.
 * The string format depends on the instance of the IStringnify.
 */
class MYSTRINGNIFY_EXPORT IStringnify {
public:
    /**
     * @brief Construct a new IStringnify object
     * 
     */
    IStringnify() = default;
    /**
     * @brief Destroy the IStringnify object
     * 
     */
    ~IStringnify() = default;
    /**
     * @brief Transforms an int.
     * 
     * @param tValue Integer to stringnify
     * @return String representation of the int.
     */
    virtual std::string process(int tValue) = 0;

    /**
     * @brief Transforms an float.
     * 
     * @param tValue Float to stringnify
     * @return String representation of the float.
     */
    virtual std::string process(float tValue) = 0;

    /**
     * @brief Transforms an double.
     * 
     * @param tValue Double to stringnify
     * @return String representation of the double.
     */
    virtual std::string process(double tValue) = 0;
};

}