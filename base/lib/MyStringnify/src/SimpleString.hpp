#pragma once

#include "IStringnify.hpp"

namespace MyString {

class SimpleString : public IStringnify {
public:
    SimpleString(const std::string& tPrefix, const std::string& tPostfix);
    virtual ~SimpleString();
    virtual std::string process(int tValue) override;
    virtual std::string process(float tValue) override;
    virtual std::string process(double tValue) override;

protected:
    std::string mPrefix;
    std::string mPostfix;
};

}