#include "MyStringnifyInterface.hpp"
#include "SimpleString.hpp"

namespace MyString {

std::unique_ptr<IStringnify> makeSimpleStringnifyer(const std::string& tPrefix,
                                                    const std::string& tPostfix)
{
    std::unique_ptr<IStringnify> ptr(new SimpleString(tPrefix, tPostfix));
    return ptr;
}

}