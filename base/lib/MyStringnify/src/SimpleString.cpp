#include "SimpleString.hpp"

namespace MyString {

SimpleString::SimpleString(const std::string& tPrefix, const std::string& tPostfix)
    : IStringnify(), mPrefix(tPrefix), mPostfix(tPostfix) {
    // Do nothing
}
    
SimpleString::~SimpleString() {

}

std::string SimpleString::process(int tValue) {
    return mPrefix + std::to_string(tValue) + mPostfix;
}

std::string SimpleString::process(float tValue) {
    return mPrefix + std::to_string(tValue) + mPostfix;
}

std::string SimpleString::process(double tValue) {
    return mPrefix + std::to_string(tValue) + mPostfix;
}

}