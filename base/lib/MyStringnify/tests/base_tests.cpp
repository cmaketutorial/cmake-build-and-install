#include "gtest/gtest.h"

#include "IStringnify.hpp"
#include "MyStringnifyInterface.hpp"

TEST( Stringnify_SimpleString, RegularNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("3", simpleString->process(3));
    EXPECT_EQ("3.000000", simpleString->process(3.0f));
    EXPECT_EQ("3.000000", simpleString->process(3.0));
}

TEST( Stringnify_SimpleString, LargeNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("31412421", simpleString->process(31412421));
    EXPECT_EQ("31412421.000000", simpleString->process(31412421.0f));
    EXPECT_EQ("31412421.000000", simpleString->process(31412421.0));
}

TEST( Stringnify_SimpleString, LargeNumbersWithFraction ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("31412421.0000000002", simpleString->process(31412421.0000000002f));
    EXPECT_EQ("31412421.0000000002", simpleString->process(31412421.0000000002));
}

TEST( Stringnify_SimpleString, NegativeNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("-3", simpleString->process(-3));
    EXPECT_EQ("-3.000000", simpleString->process(-3.0f));
    EXPECT_EQ("-3.000000", simpleString->process(-3.0));
}

TEST( Stringnify_SimpleString, LargeNegativeNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("-31412421", simpleString->process(-31412421));
    EXPECT_EQ("-31412421.000000", simpleString->process(-31412421.0f));
    EXPECT_EQ("-31412421.000000", simpleString->process(-31412421.0));
}

TEST( Stringnify_SimpleString, LargeNegativeNumbersWithFraction ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("-31412421.0000000002", simpleString->process(-31412421.0000000002f));
    EXPECT_EQ("-31412421.0000000002", simpleString->process(-31412421.0000000002));
}

TEST( Stringnify_SimpleString, FractionNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("0.1234567", simpleString->process(0.1234567f));
    EXPECT_EQ("0.1234567", simpleString->process(0.1234567));
}

TEST( Stringnify_SimpleString, NegativeFractionNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("-0.1234567", simpleString->process(0.1234567f));
    EXPECT_EQ("-0.1234567", simpleString->process(0.1234567));
}

TEST( Stringnify_SimpleString, SmallFractionNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("0.0000001234567", simpleString->process(0.0000001234567f));
    EXPECT_EQ("0.0000001234567", simpleString->process(0.0000001234567));
}

TEST( Stringnify_SimpleString, SmallNegativeFractionNumbers ) {
    auto simpleString = MyString::makeSimpleStringnifyer();
    EXPECT_EQ("-0.0000001234567", simpleString->process(-0.0000001234567f));
    EXPECT_EQ("-0.0000001234567", simpleString->process(-0.0000001234567));
}