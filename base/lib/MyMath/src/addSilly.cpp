#include "addSilly.hpp"

namespace MyMath {
    int add_numbers_silly(int tFirst, int tSecond) {
        int result = tFirst;

        for(int i = 0; i < tSecond; ++i)
        {
            ++result;
        }

        return result;
    }
}