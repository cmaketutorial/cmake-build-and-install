#include "myMath.hpp"

#include "addSilly.hpp"
#include "addSmart.hpp"

namespace MyMath {
    int addNumbersNew(int tFirst, int tSecond) {
        return add_numbers_smart(tFirst, tSecond);
    }

    int addNumbers(int tFirst, int tSecond) {
        return add_numbers_silly(tFirst, tSecond);
    }
}