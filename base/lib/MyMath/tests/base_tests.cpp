// Disable deprecated warnings in the tests since we are testing deprecated functions.
#pragma warning( push )
#pragma warning( disable : 4996 )

#include "gtest/gtest.h"

#include "myMath.hpp"

TEST( OldMath, AddSmall ) {
    EXPECT_EQ( 5, MyMath::addNumbers(3, 2) );
    EXPECT_EQ( 45, MyMath::addNumbers(1, 44) );
}

TEST( OldMath, AddNegative ) {
    EXPECT_EQ( -1, MyMath::addNumbers(-3, 2) );
    EXPECT_EQ( 1, MyMath::addNumbers(3, -2) );
    EXPECT_EQ( -5, MyMath::addNumbers(-3, -2) );
}

TEST( OldMath, AddLarge ) {
    EXPECT_EQ( 300002, MyMath::addNumbers(300000, 2) );
    EXPECT_EQ( 300002, MyMath::addNumbers(2, 300000) );
    EXPECT_EQ( 600000, MyMath::addNumbers(300000, 300000) );
}

TEST( OldMath, AddLargeNegative ) {
    EXPECT_EQ( -299998, MyMath::addNumbers(-300000, 2) );
    EXPECT_EQ( -299998, MyMath::addNumbers(2, -300000) );
    EXPECT_EQ( -600000, MyMath::addNumbers(-300000, -300000) );
}

TEST( NewMath, AddSmall ) {
    EXPECT_EQ( 5, MyMath::addNumbersNew(3, 2) );
    EXPECT_EQ( 45, MyMath::addNumbersNew(1, 44) );
}

TEST( NewMath, AddNegative ) {
    EXPECT_EQ( -1, MyMath::addNumbersNew(-3, 2) );
    EXPECT_EQ( 1, MyMath::addNumbersNew(3, -2) );
    EXPECT_EQ( -5, MyMath::addNumbersNew(-3, -2) );
}

TEST( NewMath, AddLarge ) {
    EXPECT_EQ( 300002, MyMath::addNumbersNew(300000, 2) );
    EXPECT_EQ( 300002, MyMath::addNumbersNew(2, 300000) );
    EXPECT_EQ( 600000, MyMath::addNumbersNew(300000, 300000) );
}

TEST( NewMath, AddLargeNegative ) {
    EXPECT_EQ( -299998, MyMath::addNumbersNew(-300000, 2) );
    EXPECT_EQ( -299998, MyMath::addNumbersNew(2, -300000) );
    EXPECT_EQ( -600000, MyMath::addNumbersNew(-300000, -300000) );
}

#pragma warning( pop )