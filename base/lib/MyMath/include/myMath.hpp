#pragma once

#include "mymath_export.h"

/**
 * @brief Namespace encapsulating all the functions in the library.
 * 
 */
namespace MyMath {
    /**
     * @brief Add two numbers together.
     * Here we can put a more detailed description. This description
     * can be on multiple lines. It will be added to the documentation.
     * @param tFirst First integer
     * @param tSecond Second integer
     * @return Sum of the two ints
     */
    int MYMATH_EXPORT addNumbersNew(int tFirst, int tSecond);

    /**
     * @brief Add two numbers together.
     * @deprecated Please favor the new and improved addNumbersNew.
     * @param tFirst First integer
     * @param tSecond Second integer
     * @return Sum of the two ints
     */
    int MYMATH_DEPRECATED_EXPORT addNumbers(int tFirst, int tSecond);
}