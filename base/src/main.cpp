#include <iostream>

#include "MyStringnifyInterface.hpp"
#include "IStringnify.hpp"
#include "myMath.hpp"

int main() {
    // We use both libraries to prove that the linkage worked.    
    auto stringny = MyString::makeSimpleStringnifyer("I say: ", " y'all get it?\n");

    // This should output "I say: 8 y'all get it?"
    std::cout << stringny->process(MyMath::addNumbers(3, 5));

    return 0;
}
