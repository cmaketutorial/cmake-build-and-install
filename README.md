# CMake Build and Install

## Summary
This project provides a simple project structure to get to know how to use CMake a little bit better. The libraries provided were made purely for a CMake learning perspective.

## Disclaimer
- As of now, the project has only been tested of Windows 10. Some adjustments may be necessary for Linux/Mac.
- This project includes Unit Tests which are meant to fail.

## Project Structure
- .gitignore
- README.md
- base/
    - lib/
        - googletest/
        - MyMath/
            - config/
            - include/
            - src/
            - tests/
        - MyStringnify/
            - config/
            - include/
            - src/
            - tests/
    - src/
- finished/
    - lib/
        - googletest/
        - MyMath/
        - MyStringnify/
    - src/

The base folder contains the files required to build the project, including CMake stubs.  
The finished folder contains the finished CMakeLists.txt files.  
lib contains external library dependencies.  
src contains the main app sources.  

## Dependencies
1. DemoProject
    - MyMath
    - MyStringnify
2. MyMath
    - gtest
    - gtest_main
3. MyStringnify
    - gtest
    - gtest_main
4. googletest
    - N/A

## The Goal
The goal of the project is to use CMake to generate a flexible build system. Many milestones may be reached for this project. No changes are required/allowed in the .cpp and .hpp files. Only the CMake files should be modified.

You will probably need to read some documentation on <https://cmake.org/documentation/>.
This link may also help you further on <https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/>

### 0- Helpful commands
1. cmake -G *generator-name* *path/to/project-root*  
    Generates the build system according to the generator in the current working directory.

2. cmake --build *path/to/generated-cmakefiles* --config *Debug|Release|RelWithDebInfo*  
    Build the project using the selected configuration.

3. cmake --install *path/to/generated-cmakefiles* --config *Debug|Release|RelWithDebInfo* [-D CMAKE_INSTALL_PREFIX=*target/install/path*] 
    Installs the project. You may set an install path with -D CMAKE_INSTALL_PREFIX

### 1- Project builds statically
You are able to build the full project successfully. The following files should be created in your binary folder structure the following files:
- DemoProj.exe
- MyMathTests.exe
- MyStringnifyTests.exe
- MyMath.lib
- MyStringnify.lib
- gtest.lib
- gtest_main.lib

You should be able to run all the executables with no extra work/file copying.

#### Useful keywords
- add_executable
- add_library
- add_subdirectory
- set
- target_compile_feature
- target_include_directories
- target_link_libraries
- enable_testing

### 2- Project builds dynamically
You are able to build the full project successfully. The following files should be created in your binary folder structure the following files:
- DemoProj.exe
- MyMathTests.exe
- MyStringnifyTests.exe
- MyMath.dll
- MyMath.lib
- MyStringnify.dll
- MyStringnify.lib
- gtest.dll
- gtest.lib
- gtest_main.dll
- gtest_main.lib

You should be able to run all the executables with no extra work/file copying.

#### Useful keywords
- CMAKE_LIBRARY_OUTPUT_DIRECTORY
- CMAKE_RUNTIME_OUTPUT_DIRECTORY
- GenerateExportHeader

### 3- Static library installation and import
You are able to install the *.lib* files for both MyMath and MyStringnify, along with the public headers from the *include* directories. Those must then be linked to DemoProject without rebuilding the libraries every time. The installation should only be triggerd when the install command is ran with the *--component Devel* argument.

#### Useful keywords
- install
- generator expressions
- set_target_properties
- INTERFACE_INCLUDE_DIRECTORIES

### 4- Dynamic library installation and import
You are able to install the *.lib* and *.dll* files for both MyMath and MyStringnify, along with the public headers from the *include* directories. Those must then be linked to DemoProject without rebuilding the libraries every time. You must find a way for both the static and dynamic *.lib* files to coexist. The installation should only be triggerd when the install command is ran with the *--component Devel* argument.

#### Useful keywords
- IMPORTED_IMPLIB

### 5- Export and install library targets
You are able to export the library targets. This create a *project-name*Targets.cmake file which may be included in order to import the target into a project. This may replace the code from step 3 and 4. The exported targets should automatically set the necessary properties on the imported targets. The installation should only be triggerd when the install command is ran with the *--component Devel* argument.

#### Useful keywords
- EXPORT
- include

### 6- Generate package configurations for installed libraries
You are able to install the libraries and then import them in the project using the find_package command. The package configuration files should specify a project version. The installation should only be triggered when the install command is ran with the *--component Devel* argument.

#### Useful keywords
- configure_file
- find_package
- CMakePackageConfigHelpers

#### Useful link
- <https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html>

### 7- Application install command
You are able to install the application with the required files in the case of dynamic linking. This should function wether the library targets are imported or built.

#### Useful keywords
- add_custom_command

### 8- Add quality of life improvements
The following are required for the final set of CMakeLists
- No absolute paths.
- Must be able to compile regardless of the configuration.
- Must not build the sub libraries if the packages can be found.
- Must provide an override to skip the find_package and build the libraries from source.
- In the case of dynamic linking, *.dlls* should be included to the build directory, wether the targets were built or imported.
- The choice of dynamic or static linking should be done at configuration step, not in the CMakeLists.
- Debug libraries should have a postfix to indicate the debug configuration.
- If building with the MSVC compiler, *.pdb* files should be generated and installed along with the *.dll*
- GTests should be automatically detected and runnable with the *ctest* command.

## Special Thanks
- Pablo Arias for his great ["It's time to do CMake Right"](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/) article.
